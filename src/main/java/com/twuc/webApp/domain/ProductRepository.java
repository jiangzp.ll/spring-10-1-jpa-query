package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findProductsByProductLineTextDescriptionContaining(String keyword);
    List<Product> findProductsByQuantityInStockBetween(short start, short end);
    List<Product> findProductsByQuantityInStockBetweenOrderByProductCode(short start, short end);
    List<Product> findTop3ByQuantityInStockBetweenOrderByProductCode(short start, short end);
    Page<Product> findProductsByQuantityInStockBetweenOrderByProductCode(Pageable pageable, short start, short end);
    // --end-->
}
